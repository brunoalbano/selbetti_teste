import '../styles/app.scss';
import $ from 'jquery';

//BOOSTRAP PLUGINS
import 'bootstrap/js/src/tooltip';

//BOOSTRAP TOOLTIPS
$('[data-toggle="tooltip"]').tooltip();

var steps = $('.rg-steps__item'),
	main = $('.rg-main'),
    form = $('.rg-form'),
	loader = $('.rg-loader'),
	proc = $('.rg-process'),
	finish = $('.rg-finish'),
	form_rg = $('#form-rg');

$('#btn-next').on('click', function(e){
 	e.preventDefault();

	proc.eq(0).removeClass('-active');
	proc.eq(1).addClass('-active');

	steps.eq(0).removeClass('-active').addClass('-complete');
	steps.eq(1).removeClass('-disabled').addClass('-active');

});

$('#btn-back').on('click', function(e){
	e.preventDefault();

	proc.eq(1).removeClass('-active');
	proc.eq(0).addClass('-active');

	steps.eq(0).removeClass('-complete').addClass('-active');
	steps.eq(1).removeClass('-active').addClass('-disabled');
});

form_rg.submit(function(e){
    e.preventDefault();
});

$('#btn-submit').on('click', function(e){

	steps.eq(1).removeClass('-active').addClass('-complete');
	steps.eq(2).removeClass('-disabled').addClass('-active');

	form.hide();

	loader.addClass('-active');

	setTimeout(function () {
		main.addClass('-finish');
		loader.removeClass('-active');
		finish.addClass('-active');
	}, 1800);
});



