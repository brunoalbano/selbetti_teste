# Selbetti Teste Front-end

Selbetti Teste Front-end por Bruno Albano.

### Installation

```
npm install
```
### Start Dev Server

```
npm run dev
```
### Build Prod Version

```
npm run build
```
### Live view

```
http://www.brunoalbano.com.br/testes/selbetti
```
### Features:

* ES6 Support via [babel-loader](https://github.com/babel/babel-loader)
* SASS Support via [sass-loader](https://github.com/jtangelder/sass-loader)
* Linting via [eslint-loader](https://github.com/MoOx/eslint-loader)

